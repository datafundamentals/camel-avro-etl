Camel Component Project
====================
This Project is a template of the Camel component.
When you create the component project, you need to move the META-INF/services/org/apache/camel/component/helloworld 
file to META-INF/services/org/apache/camel/component/foo where "foo" is the URI scheme for your component and any
 related endpoints created on the fly.
After I finish this need to make sure that my use of File as input does not actually read in a large file's contents but instead just holds a reference to that file and passes that name on.
	Elsewise this would be a very heavy object on large files, and I should re-design it accordingly.


For more help see the Apache Camel documentation

    http://cwiki.apache.org/CAMEL/writing-components.html
