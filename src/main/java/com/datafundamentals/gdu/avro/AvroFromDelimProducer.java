/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datafundamentals.gdu.avro;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datafundamentals.gdu.avro.internal.AvroFromDelim;

/**
 * The HelloWorld producer. A producer is the Camel abstraction that refers to
 * an entity capable of creating and sending a message to an endpoint
 */
public class AvroFromDelimProducer extends DefaultProducer {
	int i = 0;
	private static final transient Logger LOG = LoggerFactory
			.getLogger(AvroFromDelimProducer.class);
	private AvroFromDelimEndpoint endpoint;
	private boolean hasRun = false;

	public AvroFromDelimProducer(AvroFromDelimEndpoint endpoint) {
		super(endpoint);
		this.endpoint = endpoint;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 * 
	 * The processor is a core Camel concept that represents a node capable of
	 * using, creat- ing, or modifying an incoming exchange.
	 */
	public void process(Exchange exchange) throws Exception {
		/*
		 * http endpoint will run this dozens of times for some reason WTF??
		 * Needs to be fixed but not now. Probably should have used a rest call
		 * instead of an http call. Or, Claus Ibsen has suggested starting the
		 * run from a timer and not an http, which caused a change in the
		 * exchange body return type of the http for some reason. Since all this
		 * needs to change later to a proper rest call anyway, decided to leave
		 * this as is, a working POC, then fix when someone pays for this to be
		 * productionized
		 */
		if (!hasRun) {
			new AvroFromDelim().process(exchange, endpoint);
			hasRun = true;
		}
	}

}
