package com.datafundamentals.gdu.avro.internal;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.camel.RuntimeCamelException;

import com.datafundamentals.gdu.avro.AvroFromDelimEndpoint;
import com.datafundamentals.gdu.avro.DelimToAvro;

public class CallDelimToAvro {
	DelimToAvro delimToAvro = new DelimToAvro();
	AvroFromDelimEndpoint avroFromDelimEndpoint;
	File avroFile;
	File inputAsFile;
	StringBuilder errorStringBuilder = new StringBuilder();

	@SuppressWarnings("unused")
	private CallDelimToAvro() {
		// cannot be instantiated without correct parameters
	}

	public CallDelimToAvro(AvroFromDelimEndpoint avroFromDelimEndpoint,
			File avroFile, File inputAsFile) {
		this.avroFromDelimEndpoint = avroFromDelimEndpoint;
		this.avroFile = avroFile;
		this.inputAsFile = inputAsFile;
		doRun();
	}

	private void doRun() {
		if (isUseCase1()) {
			runUseCase_1_Signature();
		} else if (isUseCase2()) {
			runUseCase_2_Signature();
		} else if (isUseCase3()) {
			runUseCase_3_Signature();
		} else if (isUseCase4()) {
			runUseCase_4_Signature();
		} else {
			throw new RuntimeCamelException(
					"Unable to make a selection given parameters presented");
		}
		if (errorStringBuilder.toString() != null
				&& errorStringBuilder.toString().trim().length() > 0) {
			throw new RuntimeCamelException(errorStringBuilder.toString());
		}
	}

	private boolean isUseCase1() {
		boolean is = false;
		if ((null != avroFromDelimEndpoint.getClassName() || null != avroFromDelimEndpoint
				.getNamespace())
				&& !avroFromDelimEndpoint.isFirstLineColHeads()) {
			is = true;
		}
		return is;
	}

	private boolean isUseCase2() {
		boolean is = false;
		if (avroFromDelimEndpoint.getSchemaPath() == null
				&& null == avroFromDelimEndpoint.getClassName()
				&& null == avroFromDelimEndpoint.getNamespace()) {
			is = true;
		}
		return is;

	}

	private boolean isUseCase3() {
		boolean is = false;
		if (avroFromDelimEndpoint.getSchemaPath() != null
				&& !avroFromDelimEndpoint.isFirstLineColHeads()) {
			is = true;
		}
		return is;

	}

	private boolean isUseCase4() {
		boolean is = false;
		if (avroFromDelimEndpoint.getSchemaPath() != null
				&& avroFromDelimEndpoint.isFirstLineColHeads()) {
			is = true;
		}
		return is;

	}

	private boolean isValidForUseCase1() {
		boolean is = true;
		if (avroFromDelimEndpoint.getClassName() == null
				|| avroFromDelimEndpoint.getClassName().trim().length() < 1) {
			errorStringBuilder.append("className must be set\n");
			is = false;
		}
		if (avroFromDelimEndpoint.getNamespace() == null
				|| avroFromDelimEndpoint.getNamespace().trim().length() < 1) {
			errorStringBuilder.append("namespace must be set\n");
			is = false;
		}
		return is;

	}

	private boolean isValidForUseCase3() {
		boolean is = true;
		if (avroFromDelimEndpoint.getSchemaPath() == null
				|| avroFromDelimEndpoint.getSchemaPath().trim().length() < 1) {
			errorStringBuilder.append("schemaPath must be set\n");
			is = false;
		}

		return is;

	}

	private boolean isValidForUseCase4() {
		boolean is = true;
		if (avroFromDelimEndpoint.getSchemaPath() == null
				|| avroFromDelimEndpoint.getSchemaPath().trim().length() < 1) {
			errorStringBuilder.append("schemaPath must be set\n");
			is = false;
		}
		if (!avroFromDelimEndpoint.isFirstLineColHeads()) {
			errorStringBuilder
					.append("expecting isFirstLineColHeads() to be set true");
			is = false;
		}
		return is;

	}

	// 1
	// delimToAvro.get(inputURI, outputFile, className, namespace,
	// delimiter, exceptionOnBadData);
	private void runUseCase_1_Signature() {
		if (isValidForUseCase1()) {
			appendIfError(delimToAvro.get(inputAsFile.toURI(), avroFile,
					avroFromDelimEndpoint.getClassName(),
					avroFromDelimEndpoint.getNamespace(),
					avroFromDelimEndpoint.getDelimiter(),
					avroFromDelimEndpoint.isExceptionOnBadData()));
		}
	}

	// 2
	// delimToAvro.get(inputURI, outputFile, delimiter,
	// exceptionOnBadData)
	private void runUseCase_2_Signature() {
		appendIfError(delimToAvro.get(inputAsFile.toURI(), avroFile,
				avroFromDelimEndpoint.getDelimiter(),
				avroFromDelimEndpoint.isExceptionOnBadData()));
	}

	// 3
	// delimToAvro.get(inputFile.toURI(), schemaFile.toURI(), outputFile,
	// ",", false);
	private void runUseCase_3_Signature() {
		// no validation
		if (isValidForUseCase3()) {
			appendIfError(delimToAvro.get(inputAsFile.toURI(), getSchemaPath(),
					avroFile, avroFromDelimEndpoint.getDelimiter(),
					avroFromDelimEndpoint.isExceptionOnBadData()));
		}
	}

	private URI getSchemaPath() {
		File file = new File(avroFromDelimEndpoint.getSchemaPath());
		if(!file.exists()){
			throw new RuntimeCamelException("Schema file with path of "+ avroFromDelimEndpoint.getSchemaPath() + " does not exist");
		}
		return file.toURI();
	}

	// 4
	// delimToAvro.get(inputURI, schemaPath, outputFile, delimiter,
	// firstLineColHeads, validateSchemaFromFirstLineColHeads,
	// exceptionOnBadData)
	private void runUseCase_4_Signature() {
		if (isValidForUseCase4()) {
			appendIfError(delimToAvro.get(inputAsFile.toURI(), getSchemaPath(),
					avroFile, avroFromDelimEndpoint.getDelimiter(),
					avroFromDelimEndpoint.isFirstLineColHeads(),
					avroFromDelimEndpoint
							.isValidateSchemaFromFirstLineColHeads(),
					avroFromDelimEndpoint.isExceptionOnBadData()));
		}
	}


	private void appendIfError(String error) {
		if (error != null && error.trim().length() > 0) {
			errorStringBuilder.append(error);
		}
	}
}
