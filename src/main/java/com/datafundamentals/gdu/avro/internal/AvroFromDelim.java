package com.datafundamentals.gdu.avro.internal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.camel.Exchange;
import org.apache.camel.RuntimeCamelException;
import org.apache.camel.component.file.FileConsumer;
import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileMessage;
import org.apache.camel.converter.stream.CachedOutputStream;
import org.apache.camel.converter.stream.InputStreamCache;
import org.apache.camel.util.IOHelper;

import com.datafundamentals.gdu.avro.AvroFromDelimComponent;
import com.datafundamentals.gdu.avro.AvroFromDelimEndpoint;
import com.datafundamentals.gdu.avro.DelimToAvro;

public class AvroFromDelim {
	Exchange exchange;
	AvroFromDelimEndpoint avroFromDelimEndpoint;
	File avroFile;
	String inputAsString;
	File inputAsFile;

	public void process(Exchange exchange,
			AvroFromDelimEndpoint avroFromDelimEndpoint) throws Exception {
		this.exchange = exchange;
		this.avroFromDelimEndpoint = avroFromDelimEndpoint;
		validateDelimiter();
		createEmptyAvroFile();
		processInputType();
		loadNewFileIntoMessage();
		new CallDelimToAvro(avroFromDelimEndpoint, avroFile, inputAsFile);
	}

	private void validateDelimiter() {
		if (null == avroFromDelimEndpoint.getDelimiter()) {
			throw new IllegalArgumentException("delimiter must be set");
		} else if (!avroFromDelimEndpoint.getDelimiter().equals(",")
				&& !avroFromDelimEndpoint.getDelimiter().equals("|")
				&& !avroFromDelimEndpoint.getDelimiter().equals(";")
				&& !avroFromDelimEndpoint.getDelimiter().equals("\t")
				&& !avroFromDelimEndpoint.getDelimiter().equals(" ")) {
			throw new IllegalArgumentException("delimiter must be one of , | ; [tab] or [space] charachters, not '"+ avroFromDelimEndpoint.getDelimiter()+ "'");
		}
	}

	private void processInputType() {
		if (exchange.getIn().getBody() instanceof GenericFile<?>) {
			extractInputFile(exchange.getIn().getBody());
		} else if (exchange.getIn().getBody() instanceof InputStreamCache) {
			extractInputString(exchange.getIn().getBody());
			writeInputStringAsFile();
		} else {
			throw new IllegalArgumentException(
					AvroFromDelimComponent.NAME
							+ " requires either a GenericFile such as from file: or a InputStreamCache such as from http: for an input");
		}
	}

	private void writeInputStringAsFile() {
		// Use the avroFile path except strip the .avro
		String inputFilePath = avroFile.getPath().substring(0,
				avroFile.getPath().length() - 5);
		inputAsFile = new File(inputFilePath);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(inputAsFile);
			fileWriter.append(inputAsString);
			fileWriter.flush();
		} catch (IOException e) {
			throw new RuntimeCamelException(e);
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				throw new RuntimeCamelException(e);
			}
		}
	}

	private void extractInputFile(Object body) {
		GenericFile<File> genericFileInput = (GenericFile<File>) body;
		inputAsFile = genericFileInput.getFile();
	}

	private void extractInputString(Object contents) {
		InputStreamCache stream = (InputStreamCache) contents;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			stream.writeTo(bos);
		} catch (IOException e) {
			throw new RuntimeCamelException(e);
		}
		inputAsString = exchange.getContext().getTypeConverter()
				.convertTo(String.class, bos);
		IOHelper.close(stream, bos);
	}

	void loadNewFileIntoMessage() {
		GenericFile<File> genericFile = FileConsumer.asGenericFile(
				avroFile.getPath(), avroFile, null);
		GenericFileMessage<File> message = new GenericFileMessage<File>();
		message = new GenericFileMessage<File>(genericFile);
		message.setMessageId(genericFile.getFileName());
		exchange.setOut(message);
	}

	void createEmptyAvroFile() {
		String avroFilePath = avroFromDelimEndpoint.getOutputFilePath();
		if (avroFilePath == null || avroFilePath.length() < 7) {
			throw new RuntimeCamelException("outputFilePath of " + avroFilePath
					+ " is not valid");
		}
		if (!avroFilePath.endsWith(".avro")) {
			avroFilePath = avroFilePath + ".avro";
		}
		avroFile = new File(avroFilePath);
		avroFile.getParentFile().mkdirs();
	}
}
