/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datafundamentals.gdu.avro;

import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultEndpoint;

/**
 * Represents a HelloWorld endpoint. 
 * 
 * An endpoint is the Camel abstraction that models the end of a channel through which a system can send or receive messages. 
 */
public class AvroFromDelimEndpoint extends DefaultEndpoint {
	/* UseCase1 */
	// URI inputURI, File outputFilePath, String className,
	// String namespace, String delimiter, boolean exceptionOnBadData
	/* UseCase2 */
	// URI inputURI, File outputFilePath, String delimiter,
	// boolean exceptionOnBadData
	/* UseCase3 */
	// URI inputURI, URI schemaPath, File outputFilePath,
	// String delimiter, boolean exceptionOnBadData
	/* UseCase4 */
	// URI inputURI, URI schemaPath, File outputFilePath,
	// String delimiter, boolean firstLineColHeads,
	// boolean validateSchemaFromFirstLineColHeads,
	// boolean exceptionOnBadData

	private String inputURI;
	private String outputFilePath;
	private String className;
	private String namespace;
	private String delimiter;
	private String schemaPath;
	private boolean exceptionOnBadData=false;
	private boolean firstLineColHeads=false;
	private boolean validateSchemaFromFirstLineColHeads=false;

	public AvroFromDelimEndpoint() {
	}

	public AvroFromDelimEndpoint(String uri, AvroFromDelimComponent component) {
		super(uri, component);
	}

	public AvroFromDelimEndpoint(String endpointUri) {
		super(endpointUri);
	}

	public Producer createProducer() throws Exception {
		return new AvroFromDelimProducer(this);
	}

	public Consumer createConsumer(Processor processor) throws Exception {
		/*confuses heck out of me but does not seem needed? */
		return null;
	}

	public String getInputURI() {
		return inputURI;
	}

	public void setInputURI(String inputURI) {
		this.inputURI = inputURI;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public String getSchemaPath() {
		return schemaPath;
	}

	public void setSchemaPath(String schemaPath) {
		this.schemaPath = schemaPath;
	}

	public boolean isExceptionOnBadData() {
		return exceptionOnBadData;
	}

	public void setExceptionOnBadData(boolean exceptionOnBadData) {
		this.exceptionOnBadData = exceptionOnBadData;
	}

	public boolean isFirstLineColHeads() {
		return firstLineColHeads;
	}

	public void setFirstLineColHeads(boolean firstLineColHeads) {
		this.firstLineColHeads = firstLineColHeads;
	}

	public boolean isValidateSchemaFromFirstLineColHeads() {
		return validateSchemaFromFirstLineColHeads;
	}

	public void setValidateSchemaFromFirstLineColHeads(
			boolean validateSchemaFromFirstLineColHeads) {
		this.validateSchemaFromFirstLineColHeads = validateSchemaFromFirstLineColHeads;
	}

	public boolean isSingleton() {
		return true;
	}
}
