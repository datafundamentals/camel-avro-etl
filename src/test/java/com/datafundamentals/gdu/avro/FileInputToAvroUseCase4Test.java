/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datafundamentals.gdu.avro;

import org.apache.camel.builder.RouteBuilder;
/**
 * Corresponds to use case
 *
 */
public class FileInputToAvroUseCase4Test extends AFileInputToAvroTestSupport {


	// 4
	// delimToAvro.get(inputURI, schemaPath, outputFile, delimiter,
	// firstLineColHeads, validateSchemaFromFirstLineColHeads,
	// exceptionOnBadData)
	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new RouteBuilder() {
			public void configure() {
				//make sure noop=true for testing!
				from("file://src/test/resources?noop=true&fileName=testFile1.csv")
				.to("avroetl:abar?outputFilePath=target/output/testResults&delimiter=,&exceptionOnBadData=false&schemaPath=src/test/resources/testFile7.avsc&firstLineColHeads=true&validateSchemaFromFirstLineColHeads=true")
						.to("mock:result");
			}		};
	}
}
